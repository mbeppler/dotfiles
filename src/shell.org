-*- fn-alias: "~/.cache/emacs_tangle/aliasrc"; fn-function: "~/.cache/emacs_tangle/functionrc"; -*-
#+TITLE: shell related dotfiles
#+AUTHOR: Markus Beppler

* shell
** sh

** bash
#+begin_src sh :tangle ~/.cache/emacs_tangle/bashrc
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

[ -f "$HOME/.config/shell/common" ]      && . "$HOME/.config/shell/common"
[ -f "$HOME/.config/shell/aliasrc" ]     && . "$HOME/.config/shell/aliasrc"
[ -f "$HOME/.config/shell/functionrc" ]  && . "$HOME/.config/shell/functionrc"
[ -f "$HOME/.config/shell/bashrc_fcts" ] && . "$HOME/.config/shell/bashrc_fcts"
#+end_src
** zsh
test where to put variables needed in GUI apps
.zshrc is not visible in emacs started from =dmenu=, from shell works as expected

#+begin_src sh :tangle ~/.cache/emacs_tangle/zshenv
TSTENV1='home zsh env no export'
export TSTENV2='home zsh env with export'
#+end_src
*** from dotfiles to be split in sections
used version: 0c3af735d8f0ccf0a2c1f68cc74218abdfaf8c7b

#+begin_src sh :tangle ~/.cache/emacs_tangle/zshrc
# Enable colors and change prompt:
autoload -U colors && colors
# TODO change to my prompt --- also check
# - https://github.com/denysdovhan/spaceship-prompt
# - https://github.com/sindresorhus/pure
# for ideas/solutions

# except for the "command number \#" the prompt is like the one I use for bash
# there is no use for \# anyway so I removed it
# the history number is +2 instead of bash with +1 for the last command - why?
PS1="%B%F{blue}%D{%Y%m%d %H:%M} %(?.%F{green}.%F{red})[%? %!] %F{yellow}%n@%m:%F{magenta}%~%f%b
%# "

# History in cache directory (not saved at all as default)
# TODO save from all sessions into one file
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/shell/zsh_history
setopt INC_APPEND_HISTORY # --- save not only at exit - if not used lose history from multiple shells?

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist                   # I did not see the colours nee also to set ZLS_COLOURS (http://zsh.sourceforge.net/Doc/Release/Zsh-Modules.html#The-zsh_002fcomplist-Module)
compinit
_comp_options+=(globdots)		# Include hidden files.

###
### vi mode
###
bindkey -v
#export KEYTIMEOUT=1 since 5.2 no longer needed - could be usefull to set to 5 (defualt 2) --- wait and see

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char # vi-... will only delete till /insert/
bindkey -v '^R' history-incremental-search-backward # like with bash
bindkey -v '^N' history-incremental-search-forward # C-s is not used, but not working - add C-p for backward to be /consistant/
bindkey -v '^P' history-incremental-search-backward

# # # # # # TODO # # # # # #
# use history search
# https://unix.stackexchange.com/questions/97843/how-can-i-search-history-with-text-already-entered-at-the-prompt-in-zsh
# https://unix.stackexchange.com/questions/44115/how-do-i-perform-a-reverse-history-search-in-zshs-vi-mode
# different movement
# https://stackoverflow.com/questions/32686012/rebind-normal-mode-controls-vi-zsh
# key mapping
# https://unix.stackexchange.com/questions/116562/key-bindings-table

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# TODO compare to this solution https://superuser.com/questions/151803/how-do-i-customize-zshs-vim-mode


# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line


# Load aliases and shortcuts if existent.
[ -f "$HOME/.config/shell/common" ]       && . "$HOME/.config/shell/common"
[ -f "$HOME/.config/shell/aliasrc" ]      && . "$HOME/.config/shell/aliasrc"
[ -f "$HOME/.config/shell/functionrc" ]   && . "$HOME/.config/shell/functionrc"
[ -f "$HOME/.config/shell/bashrc_fcts" ]  && . "$HOME/.config/shell/bashrc_fcts"

# Load zsh-syntax-highlighting; should be last.
# https://github.com/zsh-users/zsh-syntax-highlighting
# source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

setopt interactivecomments
setopt auto_cd
#+end_src
*** options
man zshoptions

use =setopt= (ignoring defaults in this mode) or =unsetopt= to list the corresponding options
use =set -o= to list all options with on/off flag
**** history *move? add a history entry with all other settings*
        APPEND_HISTORY <D>
              If  this is set, zsh sessions will append their history list to the history file, rather than replace it.
              Thus, multiple parallel zsh sessions will all have the new entries from their history lists added to  the
              history  file,  in  the  order that they exit.  The file will still be periodically re-written to trim it
              when  the  number  of  lines  grows  20%  beyond  the  value  specified  by  $SAVEHIST  (see   also   the
              HIST_SAVE_BY_COPY option).

       BANG_HIST (+K) <C> <Z>
              Perform textual history expansion, csh-style, treating the character `!' specially.

       EXTENDED_HISTORY <C>
              Save each command's beginning timestamp (in seconds since the epoch) and the duration (in seconds) to the history file.  The format of this  prefixed  data
              is:

              `: <beginning time>:<elapsed seconds>;<command>'.

       HIST_ALLOW_CLOBBER
              (see redirection)

       HIST_BEEP <D>
              Beep in ZLE when a widget attempts to access a history entry which isn't there.

       HIST_EXPIRE_DUPS_FIRST
              If  the  internal history needs to be trimmed to add the current command line, setting this option will cause the oldest history event that has a duplicate
              to be lost before losing a unique event from the list.  You should be sure to set the value of HISTSIZE to a larger number than SAVEHIST in order  to  give
              you some room for the duplicated events, otherwise this option will behave just like HIST_IGNORE_ALL_DUPS once the history fills up with unique events.

       HIST_FCNTL_LOCK
              When  writing  out the history file, by default zsh uses ad-hoc file locking to avoid known problems with locking on some operating systems.  With this op‐
              tion locking is done by means of the system's fcntl call, where this method is available.  On recent operating systems this may provide better performance,
              in particular avoiding history corruption when files are stored on NFS.

       HIST_FIND_NO_DUPS
              When searching for history entries in the line editor, do not display duplicates of a line previously found, even if the duplicates are not contiguous.

       HIST_IGNORE_ALL_DUPS
              If  a  new command line being added to the history list duplicates an older one, the older command is removed from the list (even if it is not the previous
              event).

       *HIST_IGNORE_DUPS* (-h)
              Do not enter command lines into the history list if they are duplicates of the previous event.

       *HIST_IGNORE_SPACE* (-g)
              Remove command lines from the history list when the first character on the line is a space, or when one of the expanded aliases contains a  leading  space.
              Only  normal  aliases  (not global or suffix aliases) have this behaviour.  Note that the command lingers in the internal history until the next command is
              entered before it vanishes, allowing you to briefly reuse or edit the line.  If you want to make it vanish right away  without  entering  another  command,
              type a space and press return.

       HIST_LEX_WORDS
              By default, shell history that is read in from files is split into words on all white space.  This means that arguments with quoted whitespace are not cor‐
              rectly handled, with the consequence that references to words in history lines that have been read from a file may be inaccurate.  When this option is set,
              words read in from a history file are divided up in a similar fashion to normal shell command line handling.  Although this produces more accurately delim‐
              ited words, if the size of the history file is large this can be slow.  Trial and error is necessary to decide.

       HIST_NO_FUNCTIONS
              Remove function definitions from the history list.  Note that the function lingers in the internal history until the next command is entered before it van‐
              ishes, allowing you to briefly reuse or edit the definition.

       HIST_NO_STORE
              Remove the history (fc -l) command from the history list when invoked.  Note that the command lingers in the internal history until the next command is en‐
              tered before it vanishes, allowing you to briefly reuse or edit the line.

       HIST_REDUCE_BLANKS
              Remove superfluous blanks from each command line being added to the history list.

       HIST_SAVE_BY_COPY <D>
              When the history file is re-written, we normally write out a copy of the file named $HISTFILE.new and then rename it over the old one.   However,  if  this
              option is unset, we instead truncate the old history file and write out the new version in-place.  If one of the history-appending options is enabled, this
              option only has an effect when the enlarged history file needs to be re-written to trim it down to size.  Disable this only if you have special  needs,  as
              doing so makes it possible to lose history entries if zsh gets interrupted during the save.

              When  writing out a copy of the history file, zsh preserves the old file's permissions and group information, but will refuse to write out a new file if it
              would change the history file's owner.

       HIST_SAVE_NO_DUPS
              When writing out the history file, older commands that duplicate newer ones are omitted.

       HIST_VERIFY
              Whenever the user enters a line with history expansion, don't execute the line directly; instead, perform history expansion and reload the  line  into  the
              editing buffer.

       INC_APPEND_HISTORY
              This  option  works  like  APPEND_HISTORY except that new history lines are added to the $HISTFILE incrementally (as soon as they are entered), rather than
              waiting until the shell exits.  The file will still be periodically re-written to trim it when the number of lines grows 20% beyond the value specified  by
              $SAVEHIST (see also the HIST_SAVE_BY_COPY option).

       INC_APPEND_HISTORY_TIME
              This  option  is  a  variant of INC_APPEND_HISTORY in which, where possible, the history entry is written out to the file after the command is finished, so
              that the time taken by the command is recorded correctly in the history file in EXTENDED_HISTORY format.  This means that the history  entry  will  not  be
              available immediately from other instances of the shell that are using the same history file.

              This option is only useful if INC_APPEND_HISTORY and SHARE_HISTORY are turned off.  The three options should be considered mutually exclusive.

       SHARE_HISTORY <K>

              This  option  both  imports  new commands from the history file, and also causes your typed commands to be appended to the history file (the latter is like
              specifying INC_APPEND_HISTORY, which should be turned off if this option is in effect).  The  history  lines  are  also  output  with  timestamps  ala  EX‐
              TENDED_HISTORY (which makes it easier to find the spot where we left off reading the file after it gets re-written).

              By  default,  history  movement commands visit the imported lines as well as the local lines, but you can toggle this on and off with the set-local-history
              zle binding.  It is also possible to create a zle widget that will make some commands ignore imported commands, and some include them.

              If you find that you want more control over when commands get imported, you may wish to  turn  SHARE_HISTORY  off,  INC_APPEND_HISTORY  or  INC_APPEND_HIS‐
              TORY_TIME (see above) on, and then manually import commands whenever you need them using `fc -RI'.

#+begin_src sh :tangle ~/.cache/emacs_tangle/zshrc
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
#+end_src

**** TODO don't overwrite files with redirection

       CLOBBER (+C, ksh: +C) <D>
              Allows  `>'  redirection  to  truncate existing files.  Otherwise `>!' or `>|' must be used to truncate a
              file.

              If the option is not set, and the option APPEND_CREATE is also not set, `>>!' or `>>|' must  be  used  to
              create a file.  If either option is set, `>>' may be used.

       APPEND_CREATE <K> <S>
              This option only applies when NO_CLOBBER (-C) is in effect.

              If this option is not set, the shell will report an error when a append redirection (>>)  is  used  on  a
              file  that  does not already exists (the traditional zsh behaviour of NO_CLOBBER).  If the option is set,
              no error is reported (POSIX behaviour).

       HIST_ALLOW_CLOBBER *THINK*
              Add `|' to output redirections in the history.  This allows history references to clobber files even when
              CLOBBER is unset.

#+begin_src sh :tangle ~/.cache/emacs_tangle/zshrc
setopt NOCLOBBER
setopt APPEND_CREATE
unsetopt HIST_ALLOW_CLOBBER
#+end_src

* specific actions
** list directory
#+begin_src sh :tangle (progn fn-alias)
alias ll='ls -l'
alias la='ls -Al'
#+end_src

** use OS trash instead of =rm=

#+begin_src sh :tangle (if (eq system-type 'darwin) fn-function "no")
function rm() {
  fns=""
  for f in "$@"; do
    fns="$fns, POSIX file \"${PWD}/$f\""
  done

  osascript -e "tell app \"Finder\" to move {$fns} to trash"
}
#+end_src

  osascript <<EOF
-- set myList to {$fns}
repeat with f in {$fns} -- myList
    log f
end repeat
EOF

* directory shortcuts
This are specific to me. Should these be moved to an other file to prevent merge conflicts with other user. And also to not /leak/ any information?

#+begin_src sh :tangle (progn fn-alias)
alias Gnc="cd $REPOS/notes/collect/"
alias Gwd="cd $REPOS/workspace/documentation"
alias G64="cd $REPOS/C64"
alias Gbin="cd $REPOS/binary"
alias Gmd5xml="cd $REPOS/md5xml"
alias Gvm="cd ~/.config/shell/links/VMs"
alias Gct="cd $INSYNC/0005/21"
alias Gelrad="cd $INSYNC/0005/20"
alias Gard="cd $MEDIATHEK/ARD"
alias Gzdf="cd $MEDIATHEK/zdf"
alias cdmd5='cd /mnt/backup/iMac/markus/Documents/datenabgleich/md5sums'
#+end_src

* program defaults
#+begin_src sh :tangle (progn fn-alias)
alias cp='cp -i'
alias mv='mv -i'

alias sdcv="sdcv --color"
#+end_src

* TODO program shortcuts
So far these are only for MacOS. Good place to start and test the OS specific handling.
#+begin_src sh :tangle (if (eq system-type 'darwin) fn-alias "no")
alias ect='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -t'
alias ]=open
#+end_src

this is for linux (Fedora)
#+begin_src sh :tangle (if (eq system-type 'gnu/linux) fn-alias "no")
alias ect='emacsclient -t'
alias ]=xdg-open
#+end_src
* spelling correction
Use this only in very specific cases. Otherwise it is impossible to /unlearn/ the motion.

** keyboard related
*** space as shift
Pressing space together with an other key will be registered as =shift=.

In the case =SPACE= is pressed /to early/ do a correction.
#+begin_src sh :tangle (progn fn-alias)
alias cD='cd '
#+end_src

* common exports
#+begin_src sh :tangle ~/.cache/emacs_tangle/common
export PATH=~/bin:$PATH
export REPOS=~/repos
export INSYNC=/Volumes/data
export GPG_TTY=$(tty)
export PERLLIB=${REPOS}/workspace/perlscripts/system

if [ $(uname) = "Darwin" ]; then
  echo macOS
  export MEDIATHEK=/Volumes/data/0002

elif [ $(uname) = "Linux" ]; then
  echo Linux
fi
#+end_src
